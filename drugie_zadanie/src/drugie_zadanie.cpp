//============================================================================
// Name        : drugie_zadanie.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <string>
#include <iostream>
#include <fstream>
#include <math.h>
#include <random>
#include <cmath>
#include <array>
#include <stdexcept>
#include "ffft/FFTReal.h"

using namespace std;

ofstream plik;
class Packet {
protected:
	string device;
	string description;
	long date;

public:
	Packet(string device, string description, long date):
		device(device), description(description), date(date) {};

	virtual void toString(){
		cout<<"Device: "<<device<<endl;
		cout<<"Description: "<<description<<endl;
		cout<<"Date: "<<date<<endl;
	}

	virtual void sth() = 0;
	virtual ~Packet() {};
};


template <class T, int I=1024>
class Sequence : public Packet{
protected:
	int channelNr;
	string unit;
	double resolution;
	T *buffer= new T[I];

public:

	Sequence<T,I>(string device, string description, long date, int channelNr, string unit, double resolution):
		Packet(device, description, date), channelNr(channelNr), unit(unit), resolution(resolution) {};


	virtual void toString(void){
		Packet::toString();
		cout<<"ChannelNr: "<<channelNr<<endl;
		cout<<"Unit: "<<unit<<endl;
		cout<<"Resolution: "<<resolution<<endl;
		cout<<"Buffer: "<<endl;
				for(int i=0; i<I; i++)
				cout<<buffer[i]<<"\t";
				cout<<endl;
		};
	virtual ~Sequence<T,I>() {};

};

template <class T, int I=1024>
class Spectrum : public Sequence<T,I>{
protected:
	int scaling;
	string scala;

public:
	Spectrum<T,I>(string device, string description, long date, int channelNr, string unit, double resolution, int scaling):
		Sequence<T,I>(device, description, date, channelNr, unit, resolution), scaling(scaling) {};

	void toString(){
		WyborScali();
		Sequence<T,I>::toString();
		cout << "Scaling: " << scala << endl;
		cout<<endl;
	}


	void WyborScali(){
		switch(scaling){
		case 1:
			scala = "linear";
			break;
		case 2:
			scala = "logarithmic";
			break;
		default:
			scala = "Choose 1 for linear or 2 for logarithmic";
			break;
		}
	}

	virtual ~Spectrum<T,I>() {};
};




template <class T, int I=1024>
class TimeHistory : public Sequence<T,I>{
protected:
	double sensitivity;
	double srednia, Rms, odchylenie;
	T  buffer[I];
	float real[I], imag[I], modul[I];
public:
	TimeHistory<T,I>(string device, string description, long date, int channelNr, string unit, double resolution, double sensitivity, double srednia, double odchylenie):
		Sequence<T,I>(device, description, date,channelNr, unit, resolution), sensitivity(sensitivity), srednia(srednia), odchylenie(odchylenie) {};

	void Gauss(){
			std::random_device rand{};
		    std::mt19937 gen{rand()};
		    std::normal_distribution<> distribution{srednia,odchylenie};
		    try{	//obsluga poprawnosci indeksowania (operator indeksowania w klasie Sequence)
		    	for (int i=0; i<I; ++i){
		    	buffer[i]=distribution(gen);
		    	}
		    }
		    catch(int  i){
		    	cerr << "zly indeks" << i << endl;
		    }
		    catch(...){
		    	cerr << "nieznany wyjatek";
		    }
	}
	virtual	~TimeHistory<T,I>() {};

//funkcja wyliczajaca RMS
	void RMS(){
		double r=0;
		double rms[I];
	    for (int i=0; i<I; ++i){
	    	rms[i]=pow(buffer[i],2);
	    	r += rms[i];
	    }
	    Rms = sqrt(r/I);
		cout << "RMS: " << Rms << endl;
	}

//funkcja wyliczajaca FFT
	void FFT(int z){
		float f[1024];
		ffft::FFTReal<float> fftObject(1024);
		fftObject.do_fft(f,&buffer[0]);

		for(int i=0; i< I/2; i++){
			real[i] = f[i];
		}
		for(int i=1; i<= I/2; i++){
			real[I-i] = f[i];
		}
imag[0]=0.0;
imag[I/2]=0.0;
		for(int i=(I/2)+1; i<I; i++){
			imag[i-I/2]=f[i];
		}
		for(int i=(2)+1; i<=I/2; i++){
			imag[i+I/2-1]=f[I-i+1];
		}
		for(int i=0;i<I;i++){
			modul[i]=sqrt(imag[i]*imag[i]+real[i]*real[i]);
		}
		switch(z){
		case 1:
			cout << "FFT widmo zespolone:" << endl;
			for (int i=0; i<I; i++){
				cout << real[i] << " +(";
				cout << imag[i] << ")i\t";
			}
			cout << endl;
			break;
		case 2:
			cout << "FFT widmo amplitudowe:" << endl;
			for (int i=0; i<I; i++){
				cout << modul[i] << "\t";
			}
			cout << endl;
			break;
		default:
			cout << "Wybierz 1 dla widma zespolonego lub 2 dla widma amplitudowego" << endl;
			break;
		}
	}

	void toString() {
		Sequence<T,I>::toString();
		cout << "Sensitivity: " << sensitivity << endl;
		cout << "Srednia: " << srednia << endl;
		cout << "Odchylenie: " << odchylenie << endl;
		cout << "Buffer: " << buffer << endl;
			for (int i=0; i<I; i++){
				cout << buffer[i] << "\t";
			}
		cout << endl;
	}

// operator kopiowania
	void operator=(const TimeHistory<T,I>& r){
		 for (int i=0; i<I; ++i){
			buffer[i] = r.buffer[i];
		  }
	}

// operator dodawania
	void operator+(const TimeHistory<T,I>& r){
		TimeHistory<T,I> tmp("device","dodawanie", 111, 5, "unit", 0, 0, 0, 0);
		for (int i=0; i<I; ++i){
			tmp.buffer[i] = buffer[i]+r.buffer[i];
		  }
		tmp.toString();
	}

//operator dzielenia
	void operator/(const TimeHistory<T,I>& r){
		TimeHistory<T,I> tmp("device","dzielenie", 111, 6, "unit", 0, 0, 0, 0);
		for (int i=0; i<I; ++i){
			tmp.buffer[i] = buffer[i]/r.buffer[i];
			if (r.buffer[i]==0){
				string wyjatek = "dzielenie przez zero";
				throw wyjatek;
			}
		  }
		tmp.toString();
	}

// zapisywanie do pliku
	void write(){
		if(plik.is_open()){
		for(int i=0; i<I;i++){
			plik << buffer[i] << " ";
		}
		plik << endl;
		for (int i=0; i<I; i++){
			plik << real[i] << " +(";
			plik << imag[i] << ")i\t";
		}
		plik << endl;
		for(int i=0; i<I;i++){
			plik << modul[i] << " ";
		}
		plik << endl << endl;
		}
		else{
			string wyjatek = "plik nie zostal otwarty";
			throw wyjatek;
		}

	}

	void sth() {};
};


class Alarm : public Packet{
protected:
	int channelNr;
	double threshold;
	int direction;
	string kierunek;

public:
	Alarm(string device, string description, long date, int channelNr, double threshold, int direction):
		Packet(device, description, date), channelNr(channelNr), threshold(threshold), direction(direction) {};

	void toString(){
    	WyborKierunku();
 		 Packet::toString();
 		cout<<"ChannelNr: "<<channelNr<<endl;
 		cout<<"Threshold: "<<threshold<<endl;
		cout<<"Direction: "<<kierunek<<endl;
		cout<<endl;
	}

	virtual ~Alarm() {};


    void sth() {};

 	void WyborKierunku(){
		switch(direction){
		case -1:
			kierunek = "down";
			break;
		case 0:
			kierunek = "any";
			break;
		case 1:
			kierunek = "up";
			break;
		default:
			kierunek = "Choose -1 for down, 0 for any or 1 for up" ;
			break;
		}
 	 }
};

int main() {


//obsluga zakonczenia programu w okreslonym czasie
try{
	plik.open("results.txt");

		cout << "program testowy" << endl;

		cout << "TimeHistory:" << endl;
//obsluga przydzielania i zwalniania pamieci operacyjnej
try{
//inicjalizacja kan��w razem ze �redni� i odchyleniem standardowym
TimeHistory<float,1024> kanal1("device","Sygnal 1", 25, 1, "unit", 74.0, 5, 9, 0.1),
						kanal2("device","Sygnal 2", 300, 2, "unit", 5.5, 6, 10, 0.2),
						kanal3("device","Sygnal 3", 88, 3, "unit", 3.9, 7, 11, 0.3),
						kanal4("device","Sygnal 4", 119, 4, "unit", 4.5, 8, 12, 0.4);

kanal1.Gauss();
cout << "kanal1:" << endl;
kanal1.toString();
kanal1.RMS();
kanal1.FFT(1);
kanal1.FFT(2);

kanal2.Gauss();
cout << "kanal2:" << endl;
kanal2.toString();
kanal2.RMS();
kanal2.FFT(1);
kanal2.FFT(2);


kanal3.Gauss();
cout << "kanal3:" << endl;
kanal3.toString();
kanal3.RMS();
kanal3.FFT(1);
kanal3.FFT(2);

kanal4.Gauss();
cout << "kanal4:" << endl;
kanal4.toString();
kanal4.RMS();
kanal4.FFT(1);
kanal4.FFT(2);

//kopiowanie sygna�u
kanal1=kanal2;
cout << "kanal1=kanal2:" << endl;
kanal1.toString();

//sumowanie sygna��w
cout << "kanal2+kanal3:" << endl;
kanal2+kanal3;

//dzielenie sygna��w
cout << "kanal1/kanal4:" << endl;
try
{
	kanal1/kanal4;
}
catch(string w)
{
	cout << "wyjatek: " << w;
}

// zapisywanie do pliku
try{
	kanal1.write();
	kanal2.write();
	kanal3.write();
	kanal4.write();
}
catch(string w){
	cout << "wyjatek: " << w;
plik.close();
};
}
catch (std::runtime_error& e)
  {
    cout << "wyjatek: " << e.what();
  }
}
catch(std::bad_alloc& ba)
{
	cout << "wyjatek: " << ba.what();
}
	return 0;
	}

